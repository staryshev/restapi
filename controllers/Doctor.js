'use strict';

var utils = require('../utils/writer.js');
var Doctor = require('../service/DoctorService');

module.exports.addDoctor = function addDoctor (req, res, next, body) {
  Doctor.addDoctor(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.changeDoctor = function changeDoctor (req, res, next, body) {
  Doctor.changeDoctor(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findDoctorByUserId = function findDoctorByUserId (req, res, next, userId) {
  Doctor.findDoctorByUserId(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
