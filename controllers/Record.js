'use strict';

var utils = require('../utils/writer.js');
var Record = require('../service/RecordService');

module.exports.addRecord = function addRecord (req, res, next, body) {
  Record.addRecord(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.changeRecord = function changeRecord (req, res, next, body) {
  Record.changeRecord(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findRecordByCardId = function findRecordByCardId (req, res, next, cardId) {
  Record.findRecordByCardId(cardId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findRecordByDate = function findRecordByDate (req, res, next, date) {
  Record.findRecordByDate(date)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
