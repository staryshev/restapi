'use strict';


/**
 * Add a new patient
 * Add a new patient
 *
 * body Patient Add a new patient
 * returns Patient
 **/
exports.addPatient = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change patient
 * Change patient
 *
 * body Patient Change patient
 * returns Patient
 **/
exports.changePatient = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds patient by name
 * Finds patient by name
 *
 * name String name (optional)
 * returns List
 **/
exports.findPatientByName = function(name) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
}, {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds patient by surename
 * Finds patient by surename
 *
 * surename String surename (optional)
 * returns List
 **/
exports.findPatientBySurename = function(surename) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
}, {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds patient by user id
 * Finds patient by user id
 *
 * userId Integer user id (optional)
 * returns Patient
 **/
exports.findPatientByUserId = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "userId" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

