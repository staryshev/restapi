'use strict';


/**
 * Add a new doctor
 * Add a new doctor
 *
 * body Doctor Add a new doctor
 * returns Doctor
 **/
exports.addDoctor = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "position" : "Surgeon",
  "userId" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change doctor
 * Change doctor
 *
 * body Doctor Change doctor
 * returns Doctor
 **/
exports.changeDoctor = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "position" : "Surgeon",
  "userId" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds doctor by user id
 * Finds user by user id
 *
 * userId Integer user id (optional)
 * returns Doctor
 **/
exports.findDoctorByUserId = function(userId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "name" : "Ivan",
  "id" : 10,
  "surename" : "Ivanov",
  "position" : "Surgeon",
  "userId" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

