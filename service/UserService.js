'use strict';
var dbConnection = require('../dbConnection/dbConnection.js');


/**
 * Add a new user
 * Add a new user
 *
 * body User Add a new user
 * returns User
 **/
exports.addUser = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "password" : "password",
  "id" : 10,
  "login" : "login"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds user by login and password
 * Finds user by login and password
 *
 * login String Login (optional)
 * password String Password (optional)
 * returns User
 **/
exports.findUserByLoginAndPassword = function(login,password) {
  return new Promise(function(resolve, reject) {
    dbConnection.connection.query('SELECT * FROM User where (("' + login + '"=User.login) and (MD5("' + password + '")=User.password))', (err, rowsUsers, fields) => {
      if (err) throw err
      if (rowsUsers.length === 0){
        let answer = {
          fail: "Неправильный логин или пароль"
        }
        resolve(answer)
      }
      else {
        dbConnection.connection.query('SELECT * FROM Doctor where ("' + rowsUsers[0].id + '"=Doctor.userId)', (err, rowsDoctors, fields) => {
          if (err) throw err
          if (rowsDoctors.length > 0)
            resolve(rowsDoctors[0])
          else
            {
              dbConnection.connection.query('SELECT * FROM Patient where ("' + rowsUsers[0].id + '"=Patient.userId)', (err, rowsPatients, fields) => {
                if (err) throw err
                resolve(rowsPatients[0])
              })
            }
        })
      }
    })
  });
}

