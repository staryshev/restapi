'use strict';

var utils = require('../utils/writer.js');
var Card = require('../service/CardService');

module.exports.addCard = function addCard (req, res, next, body) {
  Card.addCard(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.changeCard = function changeCard (req, res, next, body) {
  Card.changeCard(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findCardByPatientId = function findCardByPatientId (req, res, next, patientId) {
  Card.findCardByPatientId(patientId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
