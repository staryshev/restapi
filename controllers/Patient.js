'use strict';

var utils = require('../utils/writer.js');
var Patient = require('../service/PatientService');

module.exports.addPatient = function addPatient (req, res, next, body) {
  Patient.addPatient(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.changePatient = function changePatient (req, res, next, body) {
  Patient.changePatient(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findPatientByName = function findPatientByName (req, res, next, name) {
  Patient.findPatientByName(name)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findPatientBySurename = function findPatientBySurename (req, res, next, surename) {
  Patient.findPatientBySurename(surename)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.findPatientByUserId = function findPatientByUserId (req, res, next, userId) {
  Patient.findPatientByUserId(userId)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
