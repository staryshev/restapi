'use strict';


/**
 * Add a new card
 * Add a new card
 *
 * body Card Add a new card
 * returns Card
 **/
exports.addCard = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "birthday" : "2000-01-23",
  "patientId" : 10,
  "adress" : "Some adress text",
  "telephone" : 79214574230,
  "id" : 10,
  "paul" : "male",
  "bloodType" : 3
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change card
 * Change card
 *
 * body Card Change card
 * returns Card
 **/
exports.changeCard = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "birthday" : "2000-01-23",
  "patientId" : 10,
  "adress" : "Some adress text",
  "telephone" : 79214574230,
  "id" : 10,
  "paul" : "male",
  "bloodType" : 3
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds card by patient id
 * Finds card by patient id
 *
 * patientId Integer patient id (optional)
 * returns Card
 **/
exports.findCardByPatientId = function(patientId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "birthday" : "2000-01-23",
  "patientId" : 10,
  "adress" : "Some adress text",
  "telephone" : 79214574230,
  "id" : 10,
  "paul" : "male",
  "bloodType" : 3
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

