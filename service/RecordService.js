'use strict';


/**
 * Add a new record
 * Add a new record
 *
 * body Record Add a new record
 * returns Record
 **/
exports.addRecord = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "treatment" : "pain",
  "doctorId" : 10,
  "cardId" : 10,
  "complaints" : "pain",
  "diagnosis" : "pain",
  "id" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Change record
 * Change record
 *
 * body Record Change record
 * returns Record
 **/
exports.changeRecord = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "treatment" : "pain",
  "doctorId" : 10,
  "cardId" : 10,
  "complaints" : "pain",
  "diagnosis" : "pain",
  "id" : 10
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds record by card id
 * Finds record by card id
 *
 * cardId Integer card id (optional)
 * returns List
 **/
exports.findRecordByCardId = function(cardId) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "treatment" : "pain",
  "doctorId" : 10,
  "cardId" : 10,
  "complaints" : "pain",
  "diagnosis" : "pain",
  "id" : 10
}, {
  "treatment" : "pain",
  "doctorId" : 10,
  "cardId" : 10,
  "complaints" : "pain",
  "diagnosis" : "pain",
  "id" : 10
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Finds record by date
 * Finds record by date
 *
 * date String date (optional)
 * returns List
 **/
exports.findRecordByDate = function(date) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "treatment" : "pain",
  "doctorId" : 10,
  "cardId" : 10,
  "complaints" : "pain",
  "diagnosis" : "pain",
  "id" : 10
}, {
  "treatment" : "pain",
  "doctorId" : 10,
  "cardId" : 10,
  "complaints" : "pain",
  "diagnosis" : "pain",
  "id" : 10
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

