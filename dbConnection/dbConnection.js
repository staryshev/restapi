'use strict';

const bp = require('body-parser')
const mysql = require('mysql')
exports.connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'qwerty',
  database: 'medicine'
})

exports.connection.connect()